<link href="{{ asset('css/payments.css') }}" rel="stylesheet" type="text/css"/>
@extends('layouts.app')
@section('content')
    <div class="container-main">
        @include('layouts.header')

                <!-- Banner - Inner
                ================================================== -->
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>Checkout</h1>
                </aside>

            </div>
        </div>


        <!-- Content - Inner
            ================================================== -->
        <div class="content-inner">


            <div class="container">
                <div id="submitted-inf" class="info-msg"></div>


                <div class="column5-10 Pright10 Pleft10 checkoutPageLeft Mbottom40 RW100-mob">
                    <div ng-show="isLoginWidgetShown()"></div>
                    <div ng-show="isProfileWidgetShown()" class="ng-hide">

                    </div>
                    <div ng-show="!isPageBlocked()" class="W100 paymentMethodContainer Mbottom20">
                        <div class="W100 F16 TAL fColorBlack Mbottom20">
                            Choose Payment Method
                            <input type="hidden" value="{!! $response->order_id !!}" name="order_id" id="order_id">

                        </div>

                        <div class="column12 Pright5 Pleft5 Ptop5 Pbottom5 RW50-mob">
                            <input type="radio" value="0" name="payment" id="payment" checked>

                            <div class="paypal-text">
                                <p>Pay with Paypal</p>
                            </div>
                            <img src="{{asset('images/payment003.jpg')}}">


                        </div>

                        <div class="column12 Pright5 Pleft5 Ptop5 Pbottom5 RW50-mob">

                            <input type="radio" value="1" name="payment" id="payment">

                            <div class="paypal-text">
                                <p>Pay with cash collection</p>
                            </div>
                            <img src="{{asset('images/payment004.jpg')}}">
                        </div>

                    </div>
                    <div ng-show="isRequestRunnerSelected() &amp;&amp; !isPageBlocked()" class="ng-hide"></div>
                    <!-- ngIf: isAppointmentSelected() --><!-- ngIf: isCashRefundSelected() -->
                    <input type="submit" class="continueBaymentbutton borderRadius3 disable paymentForm" value="Checkout &amp; Pay Now">
                </div>


                <!------------------>
                <div class="column5-10 Pright10 Pleft10 hideMob">
                    <div class="W100 paymentMethodContainer Mbottom20">
                        <div class="W100 F16 TAL fColorBlack Mbottom20">
                            Your Order Summary
                        </div>
                        <div class="W100 orderSummary bordered borderRadius5 Ptop10 Pbottom10 Pright20 Pleft20">
                            <div class="W100 TAR Mbottom5 fColorLightGrey">
                                <div ng-bind="shippingMessage" class="W100 fColorGreen F14 ng-binding">All eligible
                                    items will be shipped using Bejeeb standard service
                                </div>
                            </div>
                            <div class="W100 bordered borderRadius5 Ptop20 Pbottom30 Pright20 Pleft20 Mbottom10 orderSummaryItem Fleft">
                                <div class="column3-12 TAC Pright10">
                                </div>
                                <div class="column6-12 handleLink Pright10 Pleft10">
                                <span class="W100 fColorBlack F13 engFont">
                                    <a href="{!! $response->product_url !!}"
                                       target="_blank">{!! $response->product_url !!}</a>
                                </span>
                                    <span class="W100 fColorEfa3lyGrey F13"></span>
                                <span class="W100 fColorEfa3lyGrey F13">
                                    Quantity: {!! $response->qty !!}
                                </span>
                                </div>
                                <div class="column3-12 Pleft10 TAC Fright"><span class="W100 fColorBlack F18 Ptop10">
                                            JOD {!! $response->actual_price !!}
                                    </span></div>
                              
                            </div>
                            <div ng-controller="CouponController" ng-show="isShowCouponCodeBox()" class="ng-scope">
                                <div class="W100 F16 TAL fColorBlack Mtop20 Mbottom10 Fleft">
                                    Use Coupon code
                                </div>
                                <div class="W100 paymentCoupon openSansRegular Fleft Mbottom10">
                                    <div class="Fright"><input ng-click="applyCouponClicked()" type="button" value="GO"
                                                               class="paymentCouponButton" ng-hide="isCouponDisabled()">
                                    </div>
                                    <div class="rest"><input ng-model="coupon" type="text"
                                                             class="paymentCouponField ng-pristine ng-untouched ng-valid"
                                                             ng-disabled="isCouponDisabled()"></div>
                                </div><!-- ngIf: errorMessage || successMessage --></div>


                            <div ng-controller="TotalController" ng-show="total" class="ng-scope">
                                <!-- ngIf: total.USD.downPayment>0 --><!-- ngIf: total.USD.discount>0 -->
                                <!-- ngIf: !canShowTotalWithIntlFees && total.USD.translator.totalAfterDiscount -->
                                <div ng-if="!canShowTotalWithIntlFees &amp;&amp; total.USD.translator.totalAfterDiscount"
                                     class="W100 allerRegular Ptop20 Pbottom20 borderTop Fleft ng-scope">
                                    <div class="column8-12 TAL"><span class="W100 fColorBlack F18">Order Total</span>
                                        <!-- ngIf: showEdfa3lyTotalNotice() --><span ng-if="showEdfa3lyTotalNotice()"
                                                                                     class="W100 fColorEfa3lyGrey F14 ng-scope">(Before adding international shipping fees)</span>
                                        <!-- end ngIf: showEdfa3lyTotalNotice() --></div>
                                    <div ng-bind="total.USD.translator.totalAfterDiscount"
                                         class="column4-12 TAR fColorGreen F24 ng-binding">JOD
                                        {!! $response->total_cost !!}
                                    </div>
                                    <style>
                                        .labels {
                                            float: left;
                                            display: block;
                                            width: 40%;
                                        }

                                        .values {
                                            -moz-border-bottom-colors: none;
                                            -moz-border-left-colors: none;
                                            -moz-border-right-colors: none;
                                            -moz-border-top-colors: none;
                                            background: #2ecc71 none repeat scroll 0 0;
                                            border-image: none;
                                            border-radius: 5px;
                                            -moz-border-radius: 5px;
                                            -webkit-border-radius: 5px;
                                            color: #fff;
                                            display: block;
                                            float: left;
                                            text-align: center;
                                            width: 14%;

                                        }

                                        .charges-details {
                                            clear: both;
                                            padding-top18px;
                                            padding-bottom: 18px;
                                            display: block;
                                        }

                                        .clr {
                                            clear: both;
                                            padding-top: 2px;
                                        }
                                    </style>
                                    <div class="charges-details">
                                        <div>&nbsp;</div>
                                        <div class="clr">
                                            <div class="labels">Customs</div>
                                            <div class="values">JOD
                                                {!! $response->customs !!}</div>
                                        </div>
                                        <div class="clr">
                                            <div class="labels">Bejeeb Fees</div>
                                            <div class="values">JOD
                                                {!! $response->bejeebFees !!}</div>
                                        </div>
                                        <div class="clr">
                                            <div class="labels">Shipping Charges</div>
                                            <div class="values">JOD
                                                {!! $response->shipping_charges !!}</div>
                                        </div>
                                        <div class="clr">
                                            <div class="labels">Tax Charges</div>
                                            <div class="values">JOD
                                                {!! $response->tax_charges !!}</div>
                                        </div>
                                        <div>&nbsp;</div>
                                        <div>&nbsp;</div>
                                    </div>


                                    <!-- end ngIf: !canShowTotalWithIntlFees && total.USD.translator.totalAfterDiscount -->
                                    <!-- ngIf: canShowTotalWithIntlFees && total.USD.totalPriceAfterWeightAfterDiscount -->
                                    <!-- ngIf: total.USD.cashFees > 0 -->
                                    <!-- ngIf: !canShowTotalWithIntlFees && (total.USD.unpaid > 0 || total.USD.unpaid < 0) -->
                                    <div ng-if="!canShowTotalWithIntlFees &amp;&amp; (total.USD.unpaid > 0 || total.USD.unpaid < 0)"
                                         class="W100 allerRegular Ptop20 Pbottom20 borderTop Fleft ng-scope">
                                        <div class="column8-12 TAL"><!-- ngIf: total.USD.unpaid > 0 --><span
                                                    ng-if="total.USD.unpaid > 0" class="W100 fColorBlack F18 ng-scope">Unpaid amount</span>
                                            <!-- end ngIf: total.USD.unpaid > 0 --><!-- ngIf: total.USD.unpaid < 0 -->
                                        </div>
                                        <!-- ngIf: total.USD.unpaid > 0 -->
                                        <div ng-if="total.USD.unpaid > 0" ng-bind="total.USD.translator.unpaid"
                                             class="column4-12 TAR fColorGreen F24 ng-binding ng-scope">JOD
                                            {!! $response->total_cost !!}
                                        </div><!-- end ngIf: total.USD.unpaid > 0 --><!-- ngIf: total.USD.unpaid < 0 -->
                                    </div>
                                    <!-- end ngIf: !canShowTotalWithIntlFees && (total.USD.unpaid > 0 || total.USD.unpaid < 0) -->
                                    <!-- ngIf: canShowTotalWithIntlFees --></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            @include('layouts.footer')


        </div>
    </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


@endsection
