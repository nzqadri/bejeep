<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@extends('layouts.app')

@section('content')
<div class="container-main">
    @include('layouts.header')

    <!-- Banner - Inner
                ================================================== -->
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>My Orders</h1>
                </aside>

            </div>
        </div>



        <!-- Content - Inner
            ================================================== -->
        <div class="content-inner">
            <div class="container">

                <div class="order-details">
                    <ul>
                        <li class="head-txt">
                            <div class="col1">Order ID</div>
                            <div class="col2">Date &amp; Time</div>
                            <div class="col3">Total</div>
                            <div class="col4">Status</div>
                            <div class="col5"></div>
                        </li>

                        @foreach($ordersArray as $order)
                        <li>
                            <div class="col1">
                                <div class="title">Order ID:</div>
                                <div class="txt"><span class="red"><a href="#">#{!! $order->order_id !!}</a></span></div>
                            </div>

                            <div class="col2">
                                <div class="title">Date &amp; Time:</div>
                                <div class="txt">Placed at <span class="red">{!! date('d F Y', strtotime($order->date_created)) !!}</span>
                                    {!! date('g:i A', strtotime($order->date_created)) !!}</div>
                            </div>

                            <div class="col3">
                                <div class="title">Total:</div>
                                @if($order->total_cost>0)
                                    <div class="txt">${!! $order->total_cost !!}</div>
                                @else
                                    <div class="txt">-----</div>
                                @endif
                            </div>

                            <div style=" width: 21%;" class="col5">
                                <div class="title">Status:</div>
                                @if($order->total_cost > 0 && $order->status == 'Reviewing')
                                    <a style="padding: 5px; border: 2px solid green; border-radius: 9px; color: white; background: green none repeat scroll 0% 0%;" href="ckeckoutOrder/{!! $order->order_id !!}" id="Accepted" data-order="{!! $order->order_id !!}" >Checkout</a> <a style="padding: 5px; border: 2px solid red; border-radius: 9px; color: white; background: red none repeat scroll 0% 0%;" class="orderChange" data-order="{!! $order->order_id !!}" id="Cancelled" href="#">Cancel</a>
                                @else
                                        @if($order->total_cost > 0 && $order->status == 'Cancelled')
                                            <div class="txt"><span class="cancel">{!! $order->status !!}</span></div>
                                        @else
                                            <div class="txt"><span class="complete">{!! $order->status !!}</span></div>
                                        @endif
                                @endif
                            </div>

                            <div class="col5">
                                @if($order->total_cost>0)
                                <a href="#"
                                   data-actual="{!! $order->actual_price !!}"
                                   data-shipping="{!! $order->shipping_charges !!}"
                                   data-tax="{!! $order->tax_charges !!}"
                                   data-customs="{!! $order->customs !!}"
                                   data-fees="{!! $order->bejeebFees !!}"
                                   data-total="{!! $order->total_cost !!}"class="btn showDetail" data-toggle="modal" >
                                    Check details</a>
                                    @endif
                            </div>

                        </li>
                        @endforeach

                        {{--<li>--}}
                            {{--<div class="col1">--}}
                                {{--<div class="title">Order ID:</div>--}}
                                {{--<div class="txt"><span class="red"><a href="#"># 1231</a></span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col2">--}}
                                {{--<div class="title">Date &amp; Time:</div>--}}
                                {{--<div class="txt">Placed at <span class="red">17 august 2014</span> 1:27 AM</div>--}}
                            {{--</div>--}}

                            {{--<div class="col3">--}}
                                {{--<div class="title">Total:</div>--}}
                                {{--<div class="txt">$65 (JOD$47)</div>--}}
                            {{--</div>--}}

                            {{--<div class="col4">--}}
                                {{--<div class="title">Status:</div>--}}
                                {{--<div class="txt"><span class="cancel">Canceled</span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col5"><a href="#" class="btn">Check details</a></div>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<div class="col1">--}}
                                {{--<div class="title">Order ID:</div>--}}
                                {{--<div class="txt"><span class="red"><a href="#"># 1231</a></span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col2">--}}
                                {{--<div class="title">Date &amp; Time:</div>--}}
                                {{--<div class="txt">Placed at <span class="red">17 august 2014</span> 1:27 AM</div>--}}
                            {{--</div>--}}

                            {{--<div class="col3">--}}
                                {{--<div class="title">Total:</div>--}}
                                {{--<div class="txt">$65 (JOD$47)</div>--}}
                            {{--</div>--}}

                            {{--<div class="col4">--}}
                                {{--<div class="title">Status:</div>--}}
                                {{--<div class="txt"><span class="complete">Completed</span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col5"><a href="#" class="btn">Check details</a></div>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<div class="col1">--}}
                                {{--<div class="title">Order ID:</div>--}}
                                {{--<div class="txt"><span class="red"><a href="#"># 1231</a></span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col2">--}}
                                {{--<div class="title">Date &amp; Time:</div>--}}
                                {{--<div class="txt">Placed at <span class="red">17 august 2014</span> 1:27 AM</div>--}}
                            {{--</div>--}}

                            {{--<div class="col3">--}}
                                {{--<div class="title">Total:</div>--}}
                                {{--<div class="txt"><span class="question">(?)</span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col4">--}}
                                {{--<div class="title">Status:</div>--}}
                                {{--<div class="txt"><span class="cancel">Canceled</span></div>--}}
                            {{--</div>--}}

                            {{--<div class="col5"><a href="#" class="btn">Check details</a></div>--}}
                        {{--</li>--}}

                    </ul>
                </div>

            </div>
        </div>


        @include('layouts.footer')

        <script>
            setSelected('city', '')
        </script>

    {{--Modal--}}
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Check Detail</h4>
                    </div>
                    <div class="modal-body" >
                        <div>
                            <ul>
                                <li class="m-b-15">
                                    <strong class="col-md-4">Actual Price:</strong>
                                    <span id="actual_price" class="price"></span>
                                </li>

                                <li class="m-b-15">
                                    <strong class="col-md-4">Shipping Charges:</strong>
                                    <span id="shipping_charges" class="price"></span>
                                </li>

                                <li class="m-b-15">
                                    <strong class="col-md-4">Taxes:</strong>
                                    <span id="taxes" class="price"></span>
                                </li>

                                <li class="m-b-15">
                                    <strong class="col-md-4">Customs:</strong>
                                    <span id="customs" class="price"></span>
                                </li>

                                <li class="m-b-15">
                                    <strong class="col-md-4"><span class="tax">Bejeeb Fees:</span></strong>
                                    <span id="fees" class="price"></span>
                                </li>
                                <li class="m-b-15">
                                    <strong class="col-md-4"><span class="tax">Total:</span></strong>
                                    <span id="total" class="price"></span>
                                </li>
                                <li class="m-b-15">
                                    <br><strong class=""> &nbsp;&nbsp;&nbsp; This is the final price you will pay</strong>                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
