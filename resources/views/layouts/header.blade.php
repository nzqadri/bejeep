<header class="header-main">
    <div class="container">

        <div class="logo"><a href="{{ url('/') }}" title="Bejeeb"><img src="{{asset('images/logo-bejeeb.png')}}" alt="Bejeeb" /></a></div>

        <a class="menu-btn" data-toggle="collapse" data-target=".menu-collapse">
            <div class="icon-bar">
                <span></span>
            </div>
        </a>

        <div class="nav-bar">
            @if (Auth::guest())
                {{--<nav class="menu-collapse collapse">--}}
                <nav >
                    <ul>
                        <li><a href="{{ url('/checkout') }}"><img src="{{asset('images/icon-shopping-cart.png')}}" alt="Shoppingcart" /></a></li>
                    </ul>
                    <ul>
                        <li><a href="{{ url('/login') }}">Log in</a></li>
                    </ul>
                </nav>
            @else
                <div class="icon-bg">
                    <div class="name">{{ \Auth::user()->name  }}</div>
                    <ul>
                        <li><a href="{{ url('/checkout') }}"><img src="{{asset('images/icon-shopping-cart.png')}}" alt="Shoppingcart" /></a></li>
                        <li><a href="{{ url('/orders') }}"><i class="fa fa-list" aria-hidden="true"></i></a></li>
                        <li><a href="{{ url('/settings') }}"><img src="{{asset('images/icon-setting.png')}}" alt="Setting" /></a></li>
                        <li><a href="{{ url('/logout') }}"><img src="{{asset('images/icon-sign-out.png')}}" alt="Signout" /></a></li>
                    </ul>
                </div>
            @endif
        </div>

    </div>
</header>