<!-- Footer - Main
       ================================================== -->
<footer class="footer-main">
    <div class="container">

        <div class="contact">
            <aside>
                <figure><img src="{{asset('images/icon-phone.png')}}" alt="Phone"></figure>
                <p>+962-79-1111104 <br>
                    <span>(10am - 4pm, Sunday - Thursday</span></p>
            </aside>

            <aside>
                <figure><img src="{{asset('images/icon-mail.png')}}" alt="Phone"></figure>
                <p><a href="mailto: info@cashbashacashbasha.com">info@cashbasha.com</a></p>
            </aside>
        </div>

        <div class="footer-links">
            <div class="aside-bg">
                <aside>
                    <h4>WHO WE ARE</h4>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Why CashBasha?</a></li>
                        <li><a href="#">How It Works?</a></li>
                    </ul>
                </aside>

                <aside>
                    <h4>CUSTOMER SERVICE</h4>
                    <ul>
                        <li><a href="#">Orders &amp; Returns</a></li>
                        <li><a href="#">Shipping & Delivery</a></li>
                        <li><a href="#">Frequently Asked Questions</a></li>
                    </ul>
                </aside>

                <aside>
                    <h4>LEGAL STUFF</h4>
                    <ul>
                        <li><a href="#">Terms Of Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </aside>

                <aside>
                    <h4>STAY CONNECTED</h4>
                    <ul>
                        <li><i class="fa fa-facebook"></i><a href="#">On Facebook </a></li>
                        <li><i class="fa fa-twitter"></i><a href="#">On Twitter </a></li>
                    </ul>
                </aside>
            </div>
        </div>

    </div>
</footer>

</div>


<!-- Jquery
================================================== -->
<script src="{{asset('js/jquery.min.js')}}"></script>

<!-- Accordation
================================================== -->
<script src="{{asset('js/collapse.js')}}"></script>

<!-- Custom
================================================== -->
<script src="{{asset('js/custom.js')}}"></script>

<!-- SelectBox
================================================== -->
<script src="{{asset('js/selectBox.js')}}"></script>
<script src="{{asset('js/order.js')}}"></script>

<script>
    $(function () {
        var spCartLS = localStorage.getItem('spCart');
        var isCart = spCartLS ? 1 : 0;
        $('#localStorage').val(isCart)
    });
</script>

<script>
    addToCartBtnEvent();
</script>