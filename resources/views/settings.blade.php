@extends('layouts.app')

@section('content')
<div class="container-main">
    @include('layouts.header')
    <!-- Banner - Inner
            ================================================== -->
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>Settings</h1>
                </aside>

            </div>
        </div>


        <!-- Content - Inner
        ================================================== -->
        <div class="content-inner">
            <div class="container">

                <div class="form-block-bg">
                    <div class="form-block">
                        <div id="account-msg"></div>
                        <form action="{{ url('/updateUser') }}" method="post" name="account-update">
                            <ul>
                                <li>
                                    <label>First and Last Name</label>
                                    <div class="input-bg">
                                        <input name="firstname" id="firstname" type="text" class="input" value="{{ explode(' ',\Auth::user()->name)[0]  }}" placeholder="Benedict" />
                                    </div>

                                    <div class="input-bg">
                                        <input name="lastname" id="lastname" type="text" class="input" value="{{ explode(' ',\Auth::user()->name)[1]  }}" placeholder="Willson" />
                                    </div>
                                </li>

                                <li>
                                    <label>Email</label>
                                    <input name="email" id="email" type="text" class="input" value="{{ \Auth::user()->email  }}" placeholder="example@mail.com" />
                                </li>

                                <li>
                                    <label>Phone Number</label>
                                    <input name="phone" id="phone" type="text" class="input" value="{{ \Auth::user()->phone  }}" placeholder="+923 123 333 33 33" />
                                </li>

                                <li>
                                    <label>Shipping details</labe
                                    <input name="address_1" id="address_1" type="text" class="input" value="{{ \Auth::user()->address_1  }}" placeholder="Adress line 1:" />
                                </li>

                                <li>
                                    <input name="address_2" id="address_2" type="text" class="input" value="{{ \Auth::user()->address_2  }}" placeholder="Adress line 2:" />
                                </li>

                                <li>
                                    <div class="input-bg">
                                        <select name="city" id="city" class="select">
                                            <option value="Amman">Amman</option>
                                            <option value="Irbed">Irbed</option>
                                        </select>
                                    </div>


                                    <div class="input-bg">
                                        <select name="country" id="country" class="select">
                                            <option value="Jordan">Jordan</option>
                                        </select>
                                    </div>
                                </li>

                                <li>
                                    <label>Change password here:</label>
                                    <input name="password" id="password" type="password" class="input" placeholder="New password" />
                                </li>
                            </ul>
                                {{ csrf_field() }}
                            <button type="submit" class="btn" id="account-update-btn">Save changes</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>


        @include('layouts.footer')

        <?php
            echo "<script>setSelected('city', '')</script>";
        ?>
</div>
@endsection
