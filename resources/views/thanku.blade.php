@extends('layouts.app')
@section('content')
    <div class="container-main">
        @include('layouts.header')

                <!-- Banner - Inner
                ================================================== -->
{{--
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>Checkout</h1>
                </aside>

            </div>
        </div>

--}}

        <!-- Content - Inner
            ================================================== -->
        <div class="content-inner">
            <div class="container">

                <div class="thanyou">
                    <div class="row">
                        <div class="tx-img"><img src="{{asset('images/motorcycle_img.png')}}" alt=""></div>
                        <h2>Thank You!</h2>
                        <h2 style="margin-bottom:0;">Your order #{!! $orderId !!} has been placed and will be reviewed shortly!</h2>
                        <p>A quote with the total price will be sent to your email adddress</p>
                    </div>
                </div>

            </div>
        </div>

    </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


@endsection
