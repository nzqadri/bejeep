@extends('layouts.app')

@section('content')
    <div class="container-main">
        @include('layouts.header')
                <!-- Banner - Inner
	================================================== -->
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>Login/Sign up</h1>
                </aside>

            </div>
        </div>


        <!-- Content - Inner
        ================================================== -->
        <div class="content-inner">
            <div class="container">

                <div class="section-account">
                    <div class="account-in">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            <div class="log-in-info">
                                <h2>Log In</h2>

                                <aside>
                                    <a href="#" class="btn btn-blue">Log In with Facebook</a>

                                    <div class="divider-or"><span>or</span></div>
                                </aside>

                                <div class="form-block">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" >E-Mail</label>
                                        <input type="text" value="" name="localStorage" id="localStorage" style="display: none;">

                                            <input id="email" type="email" placeholder="example@mail.com" class="input"
                                                   name="email" value="{{ old('email') }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <span style="color: red;">{{ $errors->first('email') }}</span>
                                    </span>
                                            @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" >Password</label>

                                            <input id="password" placeholder="Password" type="password" class="input"
                                                   name="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                    </div>
                                    <ul>
                                        <li></li>
                                    </ul>

                                    <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-sign-in"></i> Login
                                            </button>

                                            <p><a href="{{ url('/password/reset') }}">Forgot Your Password?</a></p>
                                    </div>
                                </div>
                            </div>
                        </form>


                        <div class="sign-up-info">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}
                                <h2>Sign Up</h2>
                                <aside>
                                    <p>Become a CashBasha member and pay for your Amazon order with CASH! It's Easy,
                                        Secure &amp; Super Convenient!</p>
                                </aside>

                                <div class="form-block">
                                    <ul>
                                        <li>
                                            <label>First and Last Name</label>

                                            <div class="input-bg">
                                                <input name="name" type="text" class="input error"
                                                       placeholder="Benedict"/>
                                            </div>

                                            <div class="input-bg">
                                                <input name="lastname" type="text" class="input"
                                                       placeholder="Siliveister"/>
                                            </div>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </li>

                                        <li>
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">E-Mail Address</label>

                                                <input id="email" type="email" class="input" name="email"
                                                       value="{{ old('email') }}">

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </li>

                                        <li>
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Password</label>

                                                <input id="password" type="password" class="input" name="password">

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <b>{{ $errors->first('password') }}</b>
                                                    </span>
                                                @endif
                                            </div>
                                        </li>

                                        <li>
                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="password-confirm">Confirm
                                                    Password</label>

                                                    <input id="password-confirm" type="password" class="input"
                                                           name="password_confirmation">

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                                    @endif
                                            </div>
                                        </li>
                                    </ul>
                                    <button type="submit" class="btn">
                                        <i class="fa fa-btn fa-user"></i> Sign Up
                                    </button>
                                    <p>By clicking on "Sign Up", you agree to Bejeeb's <a href="#">Terms Of Service</a>
                                        and <a href="#">Privacy Policy</a>.</p>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        @include('layouts.footer')
    </div>
@endsection
