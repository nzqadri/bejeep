@extends('admin.layouts.app')

@section('content')
    <div id="adm-panels-wrap">
        <div class="panel w90" id="orders-panel">
            <div class="panel-hd">
                <span class="panel-title">Orders Management</span>

            </div>

            <div class="msg-inf" id="msg"></div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Cart</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Contact</th>
                        <th>Address</th>
                        <th>Date</th>
                    </tr>
                </thead>
                @foreach($orders as $order)
                <tr>
                    <td>{{$order->order_id}}</td>
                    <td style="cursor: pointer;"><i class="material-icons cart" onclick="orderDetails({{$order->order_id}})" id="c5">shopping_cart</i></td>
                    <td>{{$order->total_cost}}</td>
                    <td style="cursor: pointer;" order-status="{{$order->status}}" order-id="{!! $order->order_id !!}" class="status statusWin">{{$order->status}}</td>
                    <td>{{$order->name }}<br>{{$order->email}}</td>
                    <td>{{$order->address_1 . ' ' . $order->address_2 . ' ' . $order->city }}</td>
                    <td>{{$order->date_created}}</td>
                </tr>
                @endforeach
            </table>

            </div>

        </div>
    </div>
@endsection
@section('footer_scripts')


    <div class="modal fade" id="statusModel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Status</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="order-id" id="order-id" value="0"/>
                    <input type="hidden" name="order-status" id="order-status" value="0"/>

                    <select name="status" id="status">
                        <option value="Reviewing">Reviewing</option>
                        <option value="Cancelled">Cancelled</option>
                        <option value="Shipped">Shipped</option>
                        <option value="In Customs">In Customs</option>
                        <option value="Out for Delivery">Out for Delivery</option>
                        <option value="Returned">Returned</option>
                        <option value="Complete">Complete</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="statusupdate()" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->





    <!-- New modal for checkout portion.-->



    <div class="modal fade" id="checkoutModel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Details</h4>
                </div>

                <div class="modal-body" id="checkOutData"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="orderUpdate()" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




<script type="text/javascript" src="{{ asset('js/adminDashboard.js') }}"></script>

@stop

