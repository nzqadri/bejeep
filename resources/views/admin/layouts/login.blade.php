<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BeJeeb - Orders Manager</title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>



</head>
<body>

<div id="adm">
    <header>
        <div id = "company-name">BeJeep</div><div id="logout-wrap"></div>
    </header>
    <div id="adm-mid">
        @yield('content')

    @yield('footer_scripts')
</div>
</body>
</html>