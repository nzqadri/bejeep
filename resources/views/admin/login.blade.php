@extends('admin.layouts.login')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="height: 100%; margin-top:70px;">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        @if(Session('error'))
                            <div class="alert alert-danger">
                                {{ Session('error') }}
                            </div>
                        @endif
                            <form class="form-horizontal" id="admin-login" name="admin-login" method="post" action="{!! route('signin') !!}">

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">User Name</label>

                                <div class="col-md-6">
                                    <input  style="margin-bottom:0px;" type="text" name="email"  class="form-control" placeholder="Username" required="" autofocus="">

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input style="margin-bottom:0px;" type="password" name="password" class="form-control" placeholder="Password" required="">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i> Login
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
