@extends('layouts.app')

@section('content')
<div class="container-main">
    @include('layouts.header')
    <!-- Banner - Inner
            ================================================== -->
        <div class="banner-inner">
            <div class="container">

                <aside>
                    <h1>Checkout</h1>
                </aside>

            </div>
        </div>

        <!-- Content - Main
        ================================================== -->
        <div class="content-inner">
            <div class="container">

                <div class="section-checkout">
                    <div class="left-col">
                        <div class="table-info">
                            {{--<a href="#" class="icon-reply">Icon Reply</a>
                            <p>Wait, I'm not done shopping! <a href="#">Take me back to Amazon.</a></p>--}}
                            <div id="submitted-inf" class="info-msg"></div>
                            <ul id="cart-items">
                                <li class="heading">
                                    <div class="col1">Item</div>
                                    <div class="col2">Quantity</div>
                                    <div class="col3">Notes</div>
                                </li>
                            </ul>
                        </div>
                        <div class="shop-box">
                            <label>Add new item in your order</label>
                            <form method="post" action="#">
                                <div class="input-bg">
                                    <input type="text" placeholder="Paste a link to a product you like" class="input" id="product-link" name="buy">
                                    <p>&nbsp;</p>
                                </div>

                                <div class="btn-out">
                                    <button id="add-to-cart" class="btn" type="button">Add Now</button>
                                </div>
                                <br/>  <br/>  <br/>  <br/>
                            </form>
                        </div>




                        <div class="shipping-info">

                            <div class="shipping-txt">
                                <div class="icon-shipping"><img src="{{asset('images/icon-08.png')}}" alt="SHIPPING" /></div>
                                <h4>Shipping Notice</h4>
                                <p>Please note that the estimated arrival time for orders shipping from within the U.S. is 9-14 business days. If from another country, it will require more time.</p>
                                <p>Bejeeb cannot control how items ship from their sources. You may receive multiple packages that will be delivered  to you as they become available.</p>
                            </div>

                            <div class="shipping-cost">
                                {{--<ul>--}}
                                    {{--<li>--}}
                                        {{--<span>Items in Cart:</span>--}}
                                        {{--<span class="price">1</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span>Sub-Total:</span>--}}
                                        {{--<span class="price">$ 45.00</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span>US Shipping and Taxes:</span>--}}
                                        {{--<span class="price">$ 45.00</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span>Total Weight:</span>--}}
                                        {{--<span class="price">0.45 kg</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span><span class="tax">Local Customs and Taxes:</span></span>--}}
                                        {{--<span class="price">$ 45.00</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span>Fees:</span>--}}
                                        {{--<span class="price">$ 45.00</span>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<span>Total:</span>--}}
                                        {{--<span class="price">$ 45.00</span>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            </div>
                        </div>
                    </div>


                    <div class="right-col">
                        <div class="form-block">
                            <form action="#" method="post">
                                <ul>
                                   {{-- <li>
                                        <label>Cash Payment Method</label>
                                        <div class="options">
                                                <span class="check"><input type="radio" name="payment" class="styled" id="radio1" />
                                                    <label class="option-txt" for="radio1"><span class="txt-door">
                                                            By collection at my door <a href="#">How it works?</a></span></label>
                                                </span>
                                        </div>
                                    </li>

                                    <li class="code-cl">
                                        <label>If you have a coupon, you can apply it below</label>
                                        <div class="coupon-code">
                                            <input name="code" type="text" class="input" placeholder="Coupon code" />
                                            <button class="btn">Apply</button>
                                        </div>
                                    </li>--}}

                                    <li>
                                        <label>Phone Number</label>
                                        <input id="phone" name="phone" type="text" class="input" value="{{ \Auth::guest() ? '' : \Auth::user()->phone   }}" placeholder="+923 123 333 33 33" />
                                    </li>

                                    <li>
                                        <label>Shipping details</label>
                                        <input id="address1" name="address1" type="text" class="input" value="{{ \Auth::guest() ? '' : \Auth::user()->address_1 }}" placeholder="Adress line 1:" />
                                    </li>

                                    <li>
                                        <input id="address2" name="address2" type="text" class="input" value="{{ \Auth::guest() ? '' : \Auth::user()->address_2  }}" placeholder="Adress line 2:" />
                                    </li>

                                    <li>
                                        <div class="input-bg">
                                            <select name="city" id="city" class="select">
                                                <option value="Amman">Amman</option>
                                                <option value="Irbed">Irbed</option>
                                            </select>
                                        </div>

                                        <div class="input-bg">
                                            <select name="country" id="country" class="select">
                                                <option value="Jordan">Jordan</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul>

                                <button type="button" id="order-submit" class="btn" >Place my order</button>
                            </form>

                            <p>You may decide to cancel your order after it is reviewed. By placing your order, you agree to Bejeeb's <a href="#">Terms Of Service</a> and <a href="#">Privacy Policy.</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @include('layouts.footer')

        <script>
            cartItemsList();
            setSelected('city', '');
            orderSubmitBtn({!! \Auth::guest() ? 0 : \Auth::user()->id !!});
        </script>
</div>
@endsection
