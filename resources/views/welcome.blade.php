@extends('layouts.app')

@section('content')

    {{--Bej Key Web Site--}}



    <div class="container-main">

        <!-- Header
        ================================================== -->

            @include('layouts.header')


        <!-- Banner - Main
        ================================================== -->
        <div class="banner-main">
            <div class="container">

                <div class="banner-txt">
                    <h1>Bejeeb Lets you Shop on Amazon.com &amp; Pay with Cash</h1>

                    <div class="shop-box">
                        <form action="#" method="post">
                            <div class="input-bg">
                                <input name="buy" id="product-link" type="text" class="input" placeholder="Paste a link to a product you like" />
                                <p>Suggested searches: <a href="#">Clothing&amp;Shoes,</a> <a href="#">Makeup,</a> <a href="#">Books,</a> <a href="#">Video Games,</a> <a href="#">Handbags,</a> <a href="#">Kindle</a></p>
                            </div>

                            <div class="btn-out">
                                <button type="button" class="btn" id="add-to-cart">Shop Now</button>
                                <p><span>or just</span> <a href="#">browse</a></p>
                            </div>
                        </form>
                    </div>

                    <div class="bottom-arrow"><img src="images/design-arrow-01.png" alt="ARROW" /></div>
                </div>

            </div>
        </div>



        <!-- Feature - list
        ================================================== -->
        <div class="feature-list">
            <div class="container">

                <div class="article-bg">
                    <article>
                        <figure><img src="{{asset('images/icon-01.png')}}" alt="ICON" /></figure>
                        <aside>
                            <p>No Payment Cards Or Bank Required!</p>
                        </aside>
                    </article>

                    <article>
                        <figure><img src="{{asset('images/icon-02.png')}}" alt="ICON" /></figure>
                        <aside>
                            <p>Delivered Straight To Your Door</p>
                        </aside>
                    </article>

                    <article>
                        <figure><img src="{{asset('images/icon-03.png')}}" alt="ICON" /></figure>
                        <aside>
                            <p>Full Customer Support &amp; Help</p>
                        </aside>
                    </article>

                    <article>
                        <figure><img src="{{asset('images/icon-04.png')}}" alt="ICON" /></figure>
                        <aside>
                            <p>100% Secure <br /> Service</p>
                        </aside>
                    </article>
                </div>

            </div>
        </div>



        <!-- Banner - Cta
        ================================================== -->
        <div class="banner-cta">
            <div class="container">

                <div class="btn-out"><a href="#" class="btn">Learn more</a></div>

            </div>
        </div>

    @include('layouts.footer')
@endsection
