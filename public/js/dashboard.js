/***************************************************************
 *  Functions for making AJAX call
 ***************************************************************/
var xhrPool = [];
function ajaxAbortAll() {
    for (var i = 0, len = xhrPool.length; i < len; i++) {
        if (xhrPool[i]) { // IE Fix
            xhrPool[i].abort();
        }
    }
    xhrPool.length = 0;
}

function ajaxExecute(settings) {
    if (window.XMLHttpRequest && settings.URL) {
        if (!settings.URL || (settings.method !== "GET" && settings.method !== "POST")) {
            return;
        }
        if (settings.abortPending === true) {
            ajaxAbortAll();
        }
        var xhr = new XMLHttpRequest();
        var strURL = settings.URL;
        var params = settings.reqParams || null;
        if (params && settings.method === "GET") {
            params = null;
            if (typeof settings.reqParams === "object") {
                var qs = "";
                Object.keys(settings.reqParams).forEach(function (p) {
                    qs += encodeURIComponent(p) + "=" + encodeURIComponent(settings.reqParams[p]) + "&";
                });
                if (qs) {
                    strURL = settings.URL + "?" + qs.slice(0, -1);
                }
            }
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 1 && settings.addToPool) {
                xhrPool.push(xhr);
            }
            if (xhr.readyState === 4) {
                if (settings.addToPool) {
                    var hxrIndex = xhrPool.indexOf(xhr);
                    if (hxrIndex > -1) {
                        xhrPool.splice(hxrIndex, 1);
                    }
                }
                if (xhr.status === 200) {
                    var r,
                        errM = 'An error occurred';
                    if (settings.respType === 'json') {
                        try {
                            r = xhr.responseType === 'json' ? xhr.response : JSON.parse(xhr.responseText);
                            if (typeof r !== 'object' || r === null) {
                                console.log(errM);
                                return;
                            }
                        } catch (err) {
                            console.log(errM);
                            return;
                        }
                    } else {
                        r = xhr.responseText;
                    }
                    if (settings.respElemId) {
                        var respElem = document.getElementById(settings.respElemId);
                        if (respElem) {
                            respElem.innerHTML = r;
                        }
                    }
                    if (settings.callback) {
                        var fnp = settings.cbParams || [];
                        fnp.unshift(r);
                        settings.callback.apply(this, fnp);
                    }
                }
            }
        };
        xhr.open(settings.method, strURL, true);
        if (settings.reqType === 'json') {
            xhr.setRequestHeader("Content-Type", "application/json");
            if (params) {
                params = JSON.stringify(params);
            }
        }
        if (settings.respType) {
            xhr.responseType = settings.respType;
        }
        xhr.send(params);
    }
}

/******************* End AJAX functions ********************************/


/************************************************************************
    Orders
 ************************************************************************/


/*
 *
 * 
 */
function ordersList() {

    var ajaxSettings = {
        URL: "/bejeep/admin/ajax/orders-get.php",
        method: "POST",
        reqType: "json",
        respType: "json",
        reqParams: {},
        callback: function (ajResp) {
            console.log(ajResp)
            if (ajResp.err) {
                console.log(ajResp.errMsg);
            }

            ordersListShow(ajResp.orders);

        },
        addToPool: true,
        abortPending: true
    };
    ajaxExecute(ajaxSettings);
}

function ordersListShow(orders) {

    if (!Array.isArray(orders)) {
        alert('An error occurred');
        return;
    }

    var doc = document,
        ordersList = doc.getElementById('orders-list'),
        frag = doc.createDocumentFragment(),
        row,
        cartIcon,
        rowHTML;


    for (var i = 0, iLen = orders.length; i < iLen; i++) {
        row = doc.createElement('div');
        row.id = 'row' + orders[i].orderID;
        row.classList.add('list-row');

        cartIcon = doc.createElement('i');
        cartIcon.id = 'c' + orders[i].orderID;
        cartIcon.textContent = 'shopping_cart';
        cartIcon.className = 'material-icons cart';
        cartIcon.addEventListener('click', function (e) {
            cartShow(this.id.slice(1));
        }, false);

        rowHTML = '<div class="order-id">' + orders[i].orderID + '</div>';
        rowHTML += '<i id="c' + orders[i].orderID + '" class="material-icons cart">shopping_cart</i>';
        rowHTML += '<div class="total-cost" id="tc' + orders[i].orderID + '">' + orders[i].totalCost + '</div>';
        rowHTML += '<div class="status" id="' + orders[i].orderID + '" onclick="statusWin(' + orders[i].orderID + ',' + '\'' + orders[i].status + '\'' + ')">' + orders[i].status + '</div>';
        rowHTML += '<div class="contact">' + orders[i].firstName + ' ' + orders[i].lastName + '<br>' + orders[i].email + '<br>' +orders[i].phone + '</div>';
        rowHTML += '<div class="address">' + orders[i].city + ', ' + orders[i].country + '</div>';
        rowHTML += '<div class="date">' + orders[i].dateCreated + '</div>';
        row.insertAdjacentHTML('beforeend', rowHTML);
        frag.appendChild(row);
    }

    ordersList.textContent = '';
    ordersList.appendChild(frag);

    // View cart event
    var carts = ordersList.getElementsByClassName('cart');
    for (var i = 0, iLen = carts.length; i < iLen; i++) {
        carts[i].addEventListener('click', function (e) {
            cartView(this.id.slice(1));
        }, false);
    }
}

function cartView(orderID) {
    var pw = popwinCreate();
    
    var ajaxSettings = {
        URL: "/bejeep/admin/ajax/order-get.php",
        method: "POST",
        reqType: "json",
        respType: "json",
        reqParams: {'orderID': orderID},
        callback: function (ajResp) {
            if (ajResp.err !== false) {
                console.log(ajResp.errMsg);
            }

            var order = ajResp.order,
                oHTML;

            oHTML = '<div class="pw-order" id="o' + order.orderID + '">';
            oHTML += '<div id="pw-msg"></div><br>';
            oHTML += 'Order ID: ' + order.orderID + '<br>' + order.dateCreated + '<br>' + order.status + '<br><br>';
            oHTML += '<div>' + order.firstName + ' ' + order.lastName + '<br>' + order.email + '<br>Phone' + order.phone;
            oHTML += '<br>' + order.city + ', ' + order.country + '<br><br><br>';

            var prods = order.products,
                pHTML = '';

            if (Array.isArray(prods)) {
                for (var i = 0, iLen = prods.length; i < iLen; i++) {
                    pHTML += '<li>' + prods[i].url + '<div>' + prods[i].notes + '</div>';
                    pHTML += '<div>Quantity: ' + prods[i].qty + '<input type="number" step="0.1" id="p' + prods[i].orderProductID + '" value="' +prods[i].price +'"></div></li>';
                }
            }

            oHTML += '<ul id="pw-order-products">' + pHTML + '</ul>';
            oHTML += '<div><button type="button" onclick="orderUpdate(' + order.orderID + ')">Update</button></div></div>';

            pw.insertAdjacentHTML('beforeend', oHTML);

        },
        addToPool: true,
        abortPending: true
    };
    ajaxExecute(ajaxSettings);

}

function orderUpdate(orderID) {
    var inps = document.getElementById('popwin').getElementsByTagName('input'),
        i = inps.length,
        opARR = [];
    while (i--) {
        opARR.push({
            'orderProductID': inps[i].id.slice(1),
            'price': inps[i].value
        });
    }

    var ajaxSettings = {
        URL: "/bejeep/admin/ajax/order-update.php",
        method: "POST",
        reqType: "json",
        respType: "json",
        reqParams: {
            'orderID': orderID,
            'opARR': opARR
        },
        callback: function (ajResp) {
            var msgElem = document.getElementById('pw-msg'),
                totalCostElem = document.getElementById('tc' + orderID);
            if (ajResp.err !== false) {
                msgElem.textContent = 'An error occured';
                msgElem.className = 'msg-warn';
            } else {
                msgElem.textContent = 'Updated';
                msgElem.className = 'msg-inf';

                if (totalCostElem && isNumeric(ajResp.totalCost)) {
                    totalCostElem.innerHTML = ajResp.totalCost;
                }
            }
        },
        addToPool: true,
        abortPending: true
    };
    ajaxExecute(ajaxSettings);
}

function statusWin(orderID, currStatus) {
    var pw = popwinCreate(),
        pwHTML,
        statusOpts = ['Reviewing', 'Cancelled', 'Shipped', 'In Customs', 'Out for Delivery', 'Returned', 'Complete'],
        ul = '<ul>',
        radChecked;

    for (var i = 0, iLen = statusOpts.length; i < iLen; i++) {
        radChecked = statusOpts[i] === currStatus ? ' checked' : '';
        ul += '<li><input name="status" type="radio" value="' + statusOpts[i] + '"' + radChecked +'>&nbsp' + statusOpts[i] + '</li>';
    }
    ul += '</ul>';

    pwHTML = '<form id="pw-status" name="pw-status"><div id="pw-msg"></div>' + ul + '<button type="button" onclick="statusUpdate('+ orderID + ')">Update</button></form>';
    pw.insertAdjacentHTML('beforeend', pwHTML);    
}

function statusUpdate(orderID) {
    var frm = document.forms['pw-status'],
        status = frm.status.value;

    var ajaxSettings = {
        URL: "/bejeep/admin/ajax/order-status-update.php",
        method: "POST",
        reqType: "json",
        respType: "json",
        reqParams: {'orderID': orderID, 'status': status},
        callback: function (ajResp) {
            console.log(ajResp);
            var msgElem = document.getElementById('pw-msg');
            if (ajResp.err !== false) {
                msgElem.textContent = 'An error occured';
                msgElem.className = 'msg-warn';
            } else {
                msgElem.textContent = 'Updated';
                msgElem.className = 'msg-inf';

                var statusElem = document.getElementById(orderID);
                if(statusElem) {
                    statusElem.innerHTML = status
                }
            }

        },
        addToPool: true,
        abortPending: true
    };
    ajaxExecute(ajaxSettings);
}


/************************************************************************
 * Utils
 ***********************************************************************/


function isEmpty(val) {
    if (!val || (typeof val === "string" && val.trim() === "")) {
        return true;
    }
    return false;
}

function isNumeric(val) {
    if (typeof val === 'string') {
        val = val.replace(/,/g, '.');
    }
    return !isNaN(parseFloat(val)) && isFinite(val);
}

function fieldWarn(fiieldID) {
    var elem = document.getElementById(fiieldID);
    if (elem) {
        elem.classList.add('field-warn');
    }
}

function fieldsWarnClear(frm) {
    if (typeof frm !== 'object')
        return;

    var frmLen = frm.length;
    while (frmLen--) {
        frm[frmLen].classList.remove('field-warn');
    }
}






function windowDimm() {
    var a = document.documentElement,
        b = document.body,
        dm = document.getElementById('dimm'),
        dmW, dmH;

    if (!dm) {
        dm = document.createElement("div");
        dm.setAttribute("id", "dimm");
        document.body.appendChild(dm);
    }
    dmW = Math.max(a.clientWidth, a.scrollWidth, b.scrollWidth);
    dmH = Math.max(a.clientHeight, a.scrollHeight, b.scrollHeight);
    dm.setAttribute("style", "width:" + dmW + "px; height:" + dmH + "px;");
}

function windowDimmRemove() {
    var dm = document.getElementById('dimm');
    if (dm) {
        dm.parentNode.removeChild(dm);
    }
}


function popwinCreate(pos) {
    var doc = document,
        pw = doc.getElementById('popwin');

    if (pw) {
        pw.parentNode.removeChild(pw);
    }
    pw = doc.createElement('div');
    pw.setAttribute('id', 'popwin');

    /****** window position *********/

    if (pos === 'middle') {
        pw.style.opacity = 0;
        pw.style.top = 0;
    } else {
        pos = isNumeric(pos) ? parseInt(pos) : doc.documentElement.clientHeight * 15 / 100;
        pw.style.top = window.pageYOffset + pos + 'px';
    }

    // Close icon
    var iClose = doc.createElement('i');
    iClose.setAttribute('id', 'popwin-close');
    iClose.textContent = 'highlight_off';
    iClose.className = 'material-icons';
    iClose.addEventListener('click', popwinRemove, false);

    pw.appendChild(iClose);
    return doc.body.appendChild(pw);
}

function popwinMiddle() {
    var pwin = document.getElementById("popwin");
    if (pwin) {
        var pwRect = pwin.getBoundingClientRect();
        var vpH = document.documentElement.clientHeight;

        var pwTop = pwRect.height > vpH ? window.pageYOffset + 100 : (vpH - pwRect.height) / 2 + window.pageYOffset;
        pwin.style.top = pwTop + 'px';
        pwin.style.opacity = 1;
    }
}

function popwinRemove(fade) {
    var pwin = document.getElementById('popwin');
    if (pwin) {
        if (fade === true) {
            setTimeout(function () {
                Velocity(pwin, {opacity: 0}, {
                    duration: 1000, complete: function () {
                        pwin.parentNode.removeChild(pwin);
                        windowDimmRemove();
                    }
                });
            }, 2000);
        } else {
            pwin.parentNode.removeChild(pwin);
            windowDimmRemove();
        }
    }
}