
$(document).ready(function() {
	$(document).on('click', '.statusWin', function(){
		var orderID = $(this).attr('order-id');
		var currStatus = $(this).attr('order-status');
		$('#statusModel').modal('toggle');
		$("#order-id").val(orderID);
		$("#order-status").val(currStatus);
		$("#status").val(currStatus);
	})
});

 function orderDetails(order_id)
 {
	 $.ajax({
		 url: 'orderDetails',
		 type: "GET",
		 dataType: 'json',
		 data: {order_id: order_id},
	 }).done(function (orderResponse) {

		 $("#checkOutData").html(orderResponse);
		 $('#checkoutModel').modal('toggle');
	 });

 }

function statusupdate(orderID, currStatus)
{
	var orderID = $("#order-id").val();
	var currStatus = $("#order-status").val();
	var status = $("#status").val();
	$.ajax({
		url: 'statusupdate',
		type: "GET",
		dataType: 'json',
		data: {order_id: orderID,status:status},
	}).done(function (data) {
		if(data == 1){
			$('#statusModel').modal('toggle');
			window.location.href = "/dashboard";
		}
	});

}

function orderUpdate()
{

	var actual_price = $("#actual_price").val();
	var shipping_charges = $("#shipping_charges").val();
	var tax_charges = $("#tax_charges").val();
	var customs = $("#customs").val();
	var bejeebFees = $("#bejeebFees").val();
	var order_id = $("#order_id").val();
	var price = Number(actual_price) + Number(shipping_charges) + Number(tax_charges) + Number(customs) + Number(bejeebFees);


	$.ajax({
		url: 'orderUpdate',
		type: "GET",
		dataType: 'json',
		data: {bejeebFees: bejeebFees, customs: customs, order_id: order_id,actual_price:actual_price, shipping_charges: shipping_charges, tax_charges: tax_charges, price:price},
	}).done(function (data) {
		if(data == 1){
			window.location.href = "/dashboard";
		}
	});
}