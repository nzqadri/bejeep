/***************************************************************
 *  Functions for making AJAX call
 ***************************************************************/
var xhrPool = [];
function ajaxAbortAll() {
    for (var i = 0, len = xhrPool.length; i < len; i++) {
        if (xhrPool[i]) { // IE Fix
            xhrPool[i].abort();
        }
    }
    xhrPool.length = 0;
}

function ajaxExecute(settings) {
    if (window.XMLHttpRequest && settings.URL) {
        if (!settings.URL || (settings.method !== "GET" && settings.method !== "POST")) {
            return;
        }
        if (settings.abortPending === true) {
            ajaxAbortAll();
        }
        var xhr = new XMLHttpRequest();
        var strURL = settings.URL;
        var params = settings.reqParams || null;
        if (params && settings.method === "GET") {
            params = null;
            if (typeof settings.reqParams === "object") {
                var qs = "";
                Object.keys(settings.reqParams).forEach(function (p) {
                    qs += encodeURIComponent(p) + "=" + encodeURIComponent(settings.reqParams[p]) + "&";
                });
                if (qs) {
                    strURL = settings.URL + "?" + qs.slice(0, -1);
                }
            }
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 1 && settings.addToPool) {
                xhrPool.push(xhr);
            }
            if (xhr.readyState === 4) {
                if (settings.addToPool) {
                    var hxrIndex = xhrPool.indexOf(xhr);
                    if (hxrIndex > -1) {
                        xhrPool.splice(hxrIndex, 1);
                    }
                }
                if (xhr.status === 200) {
                    var r,
                        errM = 'An error occurred';
                    if (settings.respType === 'json') {
                        try {
                            r = xhr.responseType === 'json' ? xhr.response : JSON.parse(xhr.responseText);
                            if (typeof r !== 'object' || r === null) {
                                console.log(errM);
                                return;
                            }
                        } catch (err) {
                            console.log(errM);
                            return;
                        }
                    } else {
                        r = xhr.responseText;
                    }
                    if (settings.respElemId) {
                        var respElem = document.getElementById(settings.respElemId);
                        if (respElem) {
                            respElem.innerHTML = r;
                        }
                    }
                    if (settings.callback) {
                        var fnp = settings.cbParams || [];
                        fnp.unshift(r);
                        settings.callback.apply(this, fnp);
                    }
                }
            }
        };
        xhr.open(settings.method, strURL, true);
        if (settings.reqType === 'json') {
            xhr.setRequestHeader("Content-Type", "application/json");
            if (params) {
                params = JSON.stringify(params);
            }
        }
        if (settings.respType) {
            xhr.responseType = settings.respType;
        }
        xhr.send(params);
    }
}

/******************* End AJAX functions *************************/


/******************* Sign Up ************************************/
function signUpAttachEvents() {
    var btnSignUp = document.getElementById('btn-signup'),
        btnLogIn = document.getElementById('btn-login');

    if (btnSignUp) {
        btnSignUp.addEventListener('click', signUp, false);
    }

    if (btnLogIn) {
        btnLogIn.addEventListener('click', logIn, false);
    }
}

function signUp() {
    var doc = document,
        signUpWrap = doc.getElementById('signup'),
        inpElems = signUpWrap.getElementsByTagName('input'),
        msgElem = doc.getElementById('signup-msg'),
        msg = '',
        warn = false,
        firstName = doc.getElementById('firstname'),
        lastName = doc.getElementById('lastname'),
        email = doc.getElementById('email'),
        password = doc.getElementById('password');

    msgElem.textContent = '';

    for (var i = 0, iLen = inpElems.length; i < iLen; i++) {
        isEmpty(inpElems[i].value) ? inpElems[i].classList.add('error') : inpElems[i].classList.remove('error');
    }

    warn = signUpWrap.getElementsByClassName('error').length > 0 ? true : false;

    if (warn) {
        msg = 'Requuired fields are left blank';
    } else {

        if (!validEmail(email.value)) {
            warn = true;
            msg = 'Invalid email';
        }
    }

    if (msg) {
        msgElem.textContent = msg;
        return;
    } else {
        var ajaxSettings = {
            URL: 'ajax/sign-up.php',
            method: 'POST',
            reqType: 'json',
            respType: 'json',
            reqParams: {
                'firstName': firstName.value,
                'lastName': lastName.value,
                'email': email.value,
                'password': password.value
            },
            callback: function (ajResp) {
                console.log(ajResp);
                if (ajResp.err === false) {
                    window.location.href = window.location.origin + '/bejeep/settings.php';
                } else {
                    msgElem.textContent = ajResp.errMsg;
                    msgElem.className = 'warn-msg';
                }


                /*if (ajResp.err !== false || !ajaxResp.paypalURL) {
                 var msg = ajResp.errMsg || 'An error occurred';
                 msgElem.innerHTML = '<span class="msg-warn">' + msg + '</span>';
                 return;
                 }*/

                //window.location.href = '';

            }
        };
        ajaxExecute(ajaxSettings);

    }
}

function logIn() {
    var doc = document,
        loginWrap = doc.getElementById('login'),
        inpElems = loginWrap.getElementsByTagName('input'),
        msgElem = doc.getElementById('login-msg'),
        msg = '',
        warn = false,
        email = doc.getElementById('email-lg'),
        password = doc.getElementById('password-lg');

    msgElem.textContent = '';

    for (var i = 0, iLen = inpElems.length; i < iLen; i++) {
        isEmpty(inpElems[i].value) ? inpElems[i].classList.add('error') : inpElems[i].classList.remove('error');
    }

    warn = loginWrap.getElementsByClassName('error').length > 0 ? true : false;

    if (warn) {
        msg = 'Requuired fields are left blank';
    } else {
        if (!validEmail(email.value)) {
            warn = true;
            msg = 'Invalid email';
        }
    }

    if (msg) {
        msgElem.textContent = msg;
        return;
    } else {
        var ajaxSettings = {
            URL: 'ajax/log-in.php',
            method: 'POST',
            reqType: 'json',
            respType: 'json',
            reqParams: {
                'email': email.value,
                'password': password.value
            },
            callback: function (ajResp) {
                console.log(ajResp);
                if (ajResp.err === false) {
                    window.location.href = window.location.origin + '/bejeep/settings.php';
                } else {
                    msgElem.textContent = ajResp.errMsg;
                }
            }
        };
        ajaxExecute(ajaxSettings);
    }
}

/******************* Account Update *****************************/

function accountUpateAttachEvents() {
    var accUpdateBtn = document.getElementById('account-update-btn');
    if (accUpdateBtn) {
        accUpdateBtn.addEventListener('click', accountUpdate, false);
    }
}

function accountUpdate() {
    var doc = document,
        frm = doc.forms['account-update'],
        msgElem = doc.getElementById('account-msg'),
        msg = '',
        warn = false;

    fieldsWarnClear(frm);
    msgElem.textContent = '';

    var rq = ['firstname', 'lastname', 'email'];
    for (var i = 0, iLen = rq.length; i < iLen; i++) {
        if (isEmpty(frm[rq[i]].value)) {
            frm[rq[i]].classList.add('error');
            warn = true;
        }
    }

    if (warn) {
        msg = 'Requuired fields are left blank';
    } else {
        if (!validEmail(frm['email'].value)) {
            warn = true;
            msg = 'Invalid email';
        }
    }

    if (msg) {
        msgElem.textContent = msg;
        msgElem.className = 'warn-msg';
        return;
    } else {
        var ajaxSettings = {
            URL: 'ajax/account-update.php',
            method: 'POST',
            reqType: 'json',
            respType: 'json',
            reqParams: {
                'userID': frm['user-id'].value,
                'firstName': frm['firstname'].value,
                'lastName': frm['lastname'].value,
                'email': frm['email'].value,
                'phone': frm['phone'].value,
                'address1': frm['address1'].value,
                'address2': frm['address2'].value,
                'city': frm['city'].value,
                'country': frm['country'].value,
                'password': frm['password'].value,
            },
            callback: function (ajResp) {
                if (ajResp.err === true) {
                    msgElem.textContent = ajResp.errMsg;
                    msgElem.className = 'warn-msg';
                } else {
                    msgElem.textContent = ajResp.msg;
                    msgElem.className = 'info-msg';
                }
            }
        };
        ajaxExecute(ajaxSettings);
    }

}

/******************* End Account Update *************************/


/******************* Shopping cart ******************************/

function addToCartBtnEvent() {
    var addBtn = document.getElementById('add-to-cart');

    if (addBtn) {
        addBtn.addEventListener('click', addToCartWin, false);
    }

}

function addToCartWin() {
    var prodURLElem = document.getElementById('product-link');

    if (prodURLElem && prodURLElem.value) {

        var pw = popwinCreate();

        var pwHTML = '<div id="pw-cart">';
        pwHTML += '<div class="pw-section"><div class="pw-section-name">Product Item</div>';
        pwHTML += '<input type="text" id="prod-url" value="' + prodURLElem.value + '" readonly></div>';

        pwHTML += '<div class="pw-section"><div class="pw-section-name">How many you need?</div>';
        pwHTML += '<div id="pw-qty"><span class="active" data-qty="1">1</span><span data-qty="2">2</span><span data-qty="3">3</span><span data-qty="4">4</span><span data-qty="5">5</span></div></div>';

        pwHTML += '<div class="pw-section"><div class="pw-section-name">Is there a specific size, color or style, or anything you\'d like to let us know about this product?</div>';
        pwHTML += '<textarea id="prod-notes" placeholder="e.g. (Color: red, Size: small)"></textarea></div>';

        pwHTML += '<div class="pw-section"><button type="button" onclick="addToCart()">Add to cart</button></div>';
        pwHTML += '</div>';

        pw.insertAdjacentHTML('beforeend', pwHTML);

        var qtyElems = document.getElementById('pw-qty').getElementsByTagName('span'),
            i = qtyElems.length;

        while (i--) {
            qtyElems[i].addEventListener('click', setQty, false);
        }

    }
}

function setQty(e) {
    var evt = e || window.event,
        qtyWrap = document.getElementById('pw-qty');

    if (evt && qtyWrap) {
        var currTarg = e.currentTarget,
            qtyElems = qtyWrap.getElementsByTagName('span'),
            i = qtyElems.length;
        while (i--) {
            qtyElems[i].classList.remove('active');
        }
        currTarg.classList.add('active');
    }
}

function addToCart() {
    //localStorage.removeItem('spCart');
    var doc = document,
        prodURLElem = doc.getElementById('prod-url'),
        notesElem = doc.getElementById('prod-notes'),
        qtyWrap = doc.getElementById('pw-qty');

    if (prodURLElem && notesElem && qtyWrap) {
        var prodURL = prodURLElem.value,
            notes = notesElem.value,
            qty = qtyWrap.getElementsByClassName('active')[0].getAttribute('data-qty');

        var spCartLS = localStorage.getItem('spCart'),
            spCart;

        if (!spCartLS) {
            spCart = [];
            spCart[0] = {
                'prodURL': prodURL,
                'qty': qty,
                'notes': notes
            };

            localStorage.setItem('spCart', JSON.stringify(spCart));
        } else {
            spCart = JSON.parse(spCartLS);
            spCart.push({
                'prodURL': prodURL,
                'qty': qty,
                'notes': notes
            });
            localStorage.setItem('spCart', JSON.stringify(spCart))
        }
        window.location.href = window.location.origin + '/checkout';
    }
}

function cartItemsList() {

    var doc = document,
        cartItemsUL = doc.getElementById('cart-items'),
        spCartLS = localStorage.getItem('spCart');

    console.log(spCartLS);

    if (spCartLS) {
        var spCart = JSON.parse(spCartLS),
            frag = doc.createDocumentFragment(),
            li,
            liHTML;

        if (Array.isArray(spCart)) {
            for (var i = 0, iLen = spCart.length; i < iLen; i++) {
                li = doc.createElement('li');
                li.id = 'item' + i;

                liHTML = '<div class="col1">';
                liHTML += '<figure><a href="#"><img src="images/photo-bejeeb-sml-01.jpg" alt="PHOTO"></a></figure>';
                liHTML += '<aside><p>' + spCart[i].prodURL + '<br><a href="javascript:void(0)" onclick="removeFromCart(' + i + ')">Remove Item</a></p></aside></div>';

                liHTML += '<div class="col2">' + spCart[i].qty + '</div>';
                liHTML += '<div class="col3">' + spCart[i].notes + '</div>';

                li.innerHTML = liHTML;
                frag.appendChild(li);
            }

            cartItemsUL.appendChild(frag);

        }
    }
}

function removeFromCart(spI) {
    var spCartLS = localStorage.getItem('spCart');
    if (spCartLS) {
        try {
            var spCart = JSON.parse(spCartLS);
            if (Array.isArray(spCart)) {
                spCart.splice(spI, 1);
                localStorage.setItem('spCart', JSON.stringify(spCart));
                var prodLI = document.getElementById('item' + spI);
                if (prodLI) {
                    prodLI.parentNode.removeChild(prodLI);
                }
            }
        } catch (err) {
            localStorage.removeItem('spCart');
        }

    }
}

/******************* Shopping cart ******************************/

/******************* Orders *************************************/

function orderSubmitBtn(isLogged) {
    var orderSubmitBtn = document.getElementById('order-submit');
    if (orderSubmitBtn) {
        if (isLogged > 0) {
            orderSubmitBtn.addEventListener('click', orderSubmit, false);
        } else {
            orderSubmitBtn.addEventListener('click', function (e) {
                window.location.href = window.location.origin + '/login';
            }, false);
        }
    }

}

function orderSubmit() {
    var spCartLS = localStorage.getItem('spCart');
    if (spCartLS) {
        var spCart = JSON.parse(spCartLS);
        //console.log(spCart);
        if (!Array.isArray(spCart) || spCart.length === 0) {
            return;
        }

        var phone = $('#phone').val();
        var address1 = $('#address1').val();
        var address2 = $('#address2').val();


        $.ajax({
            type: "POST",
            url: "/orderSubmit",
            // The key needs to match your method's input parameter (case-sensitive).
            data: JSON.stringify({ Markers: spCart,phone:phone,address1:address1,address2:address2 }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){

                localStorage.removeItem('spCart');
                var cartUL = document.getElementById('cart-items'),
                    cartLI = cartUL.getElementsByTagName('li'),
                    i = cartLI.length;

                while (i--) {
                    if (!cartLI[i].classList.contains('heading')) {
                        cartUL.removeChild(cartLI[i]);
                    }
                }
                // $('#submitted-inf').html('Your order have been submited');
                // console.log(data);
                window.location.href = '/thanku/'+data;
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    }
}

/******************* End Orders *********************************/



/******* Utils *******/

function isEmpty(val) {
    if (!val || (typeof val === "string" && val.trim() === "")) {
        return true;
    }
    return false;
}

function isNumeric(val) {
    if (typeof val === 'string') {
        val = val.replace(/,/g, '.');
    }
    return !isNaN(parseFloat(val)) && isFinite(val);
}

function validEmail(str) {
    if (typeof str !== "string")
        return false;
    var val = str.trim();
    var atpos = val.indexOf("@");
    var dotpos = val.lastIndexOf(".");
    if (val.indexOf(" ") !== -1 || atpos < 1 || dotpos < 1) {
        return false;
    } else if (val.indexOf("@", (atpos + 1)) !== -1) {
        return false;
    } else if ((dotpos - atpos) < 2 || (val.length - (dotpos + 1)) < 2) {
        return false;
    } else {
        return true;
    }
}

function fieldsWarnClear(f) {
    if (typeof f !== 'object')
        return;
    var fLen = f.length;
    while (fLen--) {
        f[fLen].classList.remove('error');
    }
}

function setSelected(elemID, val) {
    if (!elemID || !val) {
        return;
    }
    var selectElem = document.getElementById(elemID);

    if (selectElem) {
        for (var i = 0, iLen = selectElem.options.length; i < iLen; i++) {
            if (selectElem.options[i].value === val) {
                selectElem.selectedIndex = i;
                break;
            }
        }
    }

}

// Create pop-up window
function popwinCreate(pos) {
    var doc = document,
        pw = doc.getElementById('popwin');

    if (pw) {
        pw.parentNode.removeChild(pw);
    }
    pw = doc.createElement('div');
    pw.setAttribute('id', 'popwin');

    /****** window position *********/

    if (pos === 'middle') {
        pw.style.opacity = 0;
        pw.style.top = 0;
    } else {
        pos = isNumeric(pos) ? parseInt(pos) : doc.documentElement.clientHeight * 15 / 100;
        pw.style.top = window.pageYOffset + pos + 'px';
    }

    // Close icon
    var iClose = doc.createElement('i');
    iClose.setAttribute('id', 'popwin-close');
    iClose.className = 'fa fa-times-circle';
    iClose.addEventListener('click', popwinRemove, false);

    pw.appendChild(iClose);
    return doc.body.appendChild(pw);
}

// Remove pop-up window
function popwinRemove() {
    var pwin = document.getElementById('popwin');

    if (pwin) {
        pwin.parentNode.removeChild(pwin);
    }
}