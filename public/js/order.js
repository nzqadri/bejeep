$(document).ready(function() {

	$(document).on('click', '.showDetail', function(e) {

		$('#actual_price').html($(this).attr('data-actual')+' (JOD)');
		$('#shipping_charges').html($(this).attr('data-shipping')+' (JOD)');
		$('#taxes').html($(this).attr('data-tax')+' (JOD)');
		$('#customs').html($(this).attr('data-customs')+' (JOD)');
		$('#fees').html($(this).attr('data-fees')+' (JOD)');
		$('#total').html($(this).attr('data-total')+' (JOD)');
		$("#myModal").modal("show");
	});


	$(document).on('click', '.paymentForm', function(e) {

		var payment = $("input[name=payment]:checked").val();
		var order_id = $("#order_id").val();

		$.ajax({
			url: '/orderConfirmed',
			type: "GET",
			dataType: 'json',
			data: {payment: payment, order_id: order_id},
		}).done(function (data) {
			if(data == 1){
				$('#submitted-inf').html('Your order have been Updated!');
				setTimeout(window.location.href = "/orders", 2000);
			}
			//setTimeout(window.location.href = "/orders", 5000);

		});

	});



	$(document).on('click', '.orderChange', function(e) {
		var orderStatus = $(this).attr('id');
		var orderNumber = $(this).attr('data-order');
		$.ajax({
			type: "POST",
			url: "/orderChangeStatus",
			data: { orderStatus: orderStatus,orderNumber:orderNumber },
			success: function(data){
				if(orderStatus == 'Cancelled') {
					$('#submitted-inf').html('Your order have been Cancelled!');
					window.location.href = window.location.origin + '/orders';
				}
			},
			failure: function(errMsg) {
				alert(errMsg);
			}
		});
	});
});

