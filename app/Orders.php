<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    public $timestamps = false;

    protected $fillable = [
        'order_id','uid', 'total_cost', 'status', 'date_created'
    ];
}
