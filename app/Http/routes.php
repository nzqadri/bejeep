<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

/**
 * Controller For admin login
 */
Route::get('admin', array('as' => 'admin', 'uses' => 'AdminController@index'));
Route::post('signin',  array('as' => 'signin', 'uses' => 'AdminController@signin'));

Route::group(['middleware' => 'admin'],function(){

    Route::get('dashboard',  array('as' => 'dashboard', 'uses' => 'AdminController@dashboard'));

    Route::get('statusupdate',  array('as' => 'statusupdate', 'uses' => 'AdminController@statusupdate'));

    Route::get('orderDetails',  array('as' => 'orderDetails', 'uses' => 'AdminController@orderDetails'));
    Route::get('orderUpdate',  array('as' => 'orderUpdate', 'uses' => 'AdminController@orderUpdate'));

    Route::get('Adminlogout',  array('as' => 'Adminlogout', 'uses' => 'AdminController@logout'));
});


Route::get('/home', 'HomeController@index');
Route::get('/checkout', 'HomeController@checkout');



//Route::group(['middleware' => 'auth'], function () {



Route::get('orderConfirmed',  array('as' => 'orderConfirmed', 'uses' => 'HomeController@orderConfirmed'));

Route::get('ckeckoutOrder/{order_id}',  array('as' => 'ckeckoutOrder', 'uses' => 'HomeController@ckeckoutOrder'));



Route::get('getProductImage',  array('as' => 'getProductImage', 'uses' => 'HomeController@getProductImage'));
Route::get('thanku/{order_id}',  array('as' => 'thanku', 'uses' => 'HomeController@thanku'));



Route::get('/settings', 'HomeController@settings');
    Route::post('/updateUser', 'HomeController@updateUser');
    Route::get('/orders', 'HomeController@orders');
    Route::post('/orderSubmit', 'HomeController@orderSubmit');
    Route::post('/orderChangeStatus', 'HomeController@orderChangeStatus');

//});