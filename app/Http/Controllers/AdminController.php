<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.login');
    }


    /*
     * Login Method for the admin
     */

    public function signin(Request $request)
    {

        $select = ['uname'];
        $query = DB::table('admins')
            ->where('email', '=',  $request->get('email'))
            ->where('password', '=', md5($request->get('password')))
            ->select($select)
            ->first();
        if(count($query) > 0) {
            Session::set('admin', $query->uname);
            return Redirect::route('dashboard');
        }
        return redirect()->back()->withError('Invalid user name or password');
    }

    public function dashboard ()
    {
        $select = ['order_id', 'users.id', 'total_cost', 'status', 'date_created', 'address_1', 'address_2', 'city', 'country', 'name', 'email'];
        $ordersQuery = DB::table('orders')
            ->join('users', 'users.id', '=', 'orders.uid')
            ->select($select)
            ->orderBy('orders.order_id', 'desc')
            ->get();
        $orders = $ordersQuery;
        if(count($ordersQuery) > 0)
            return view('admin.dashboard', compact('orders'));
    }

    public function statusupdate(Request $request)
    {
        $query = DB::table('orders')
            ->where('order_id', '=',  $request->get('order_id'))
            ->update(['status' => $request->get('status')]);
        if($query)
            return 1;
    }

    public function orderDetails(Request $request)
    {
        $select = ['product_url', 'customs', 'bejeebFees', 'qty', 'price', 'notes', 'actual_price', 'shipping_charges', 'tax_charges',  'orders_products.order_id', 'users.id', 'total_cost', 'status', 'date_created', 'address_1', 'address_2', 'city', 'country', 'name', 'email'];
        $ordersQuery = DB::table('orders')
            ->join('users', 'users.id', '=', 'orders.uid')
            ->join('orders_products', 'orders_products.order_id', '=', 'orders.order_id')
            ->select($select)
            ->where('orders_products.order_id', '=',  $request->get('order_id'))
            ->get();
        $order = $ordersQuery;

        $HTML = '';
        $HTML .= '<div class="row">';
            $HTML .= '<div class="col-md-12"> <a target="_blank" href='.$order[0]->product_url.'>URL :<strong>'.$order[0]->product_url.'</strong></a></div>';
            $HTML .= '<br><br>';
            $HTML .= '<input  type="hidden" name="order_id" id="order_id" value="'.$order[0]->order_id.'">';
        $HTML .= '</div>';

        $HTML .= '<div class="row">';
            $HTML .= '<br><br>';
            $HTML .= '<div class="col-md-2">Quantity :<strong>'.$order[0]->qty.'</strong></div>';
            $HTML .= '<div class="col-md-2"><strong>'.$order[0]->notes.'</strong></div>';
            $HTML .= '<div class="col-md-3">Status :<strong>'.$order[0]->status.'</strong></div>';
            $HTML .= '<div class="col-md-5">Requested by : <strong>' . $order[0]->name. '</strong></div>';
        $HTML .= '</div>';
        $HTML .= '<div class="row">';
            $HTML .= '<br>';
            $HTML .= '<div class="col-md-4"><lable>Actual Cost</lable><input type="text" name="actual_price" id="actual_price" value="'.$order[0]->actual_price .'"></div>';
            $HTML .= '<div class="col-md-4"><lable>Shipping Charges</lable><input type="text" name="shipping_charges" id="shipping_charges" value="'.$order[0]->shipping_charges .'"></div>';
            $HTML .= '<div class="col-md-4"><lable>Tax Charges</lable><input type="text" name="tax_charges" id="tax_charges" value="'.$order[0]->tax_charges .'"></div>';
        $HTML .= '</div>';
        $HTML .= '</br>';
        $HTML .= '<div class="row">';
        $HTML .= '<div class="col-md-4"><lable>Customs</lable><input type="text" name="customs" id="customs" value="'.$order[0]->customs .'"></div>';
            $HTML .= '<div class="col-md-4"><lable>Bejeeb fees</lable><input type="text" name="bejeebFees" id="bejeebFees" value="'.$order[0]->bejeebFees .'"></div>';

        $HTML .= '</div>';

        $HTML .= '<div class="row">';
        $HTML .= '<br>';
            $HTML .= '<div class="col-md-6"><span style="color: #ac2925;">Grand Total :<strong> JOD '.$order[0]->price.'</strong></span></div>';
        $HTML .= '</div>';


        return json_encode($HTML, true);
    }


    public function orderUpdate(Request $request)
    {
        $query = DB::table('orders_products')
            ->where('order_id', '=',  $request->get('order_id'))
            ->update($request->input());

        $query = DB::table('orders')
            ->where('order_id', '=',  $request->get('order_id'))
            ->update(['total_cost' =>$request->input('price')]);

        if($query)
            return 1;
    }


    public function logout()
    {
        session()->flush();
        return Redirect::route('admin');
    }
}
