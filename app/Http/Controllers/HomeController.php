<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Orders;
use App\OrdersProducts;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User();
        $this->orders = new Orders();
        $this->ordersProducts = new OrdersProducts();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function checkout(){
        return view('checkout');
    }

    public function settings(){
        return view('settings');
    }

    public function updateUser(Request $request)
    {
        $firstname = $request['firstname'] ? $request['firstname'] : '';
        $lastname = $request['lastname'] ? $request['lastname'] : '';
        $name = $firstname . ' ' . $lastname;
        $obj = ([
            'name' => $name ? $name : NULL,
            'phone'=> $request['phone'] ? $request['phone'] : NULL,
            'address_1' => $request['address_1'] ? $request['address_1'] : NULL,
            'address_2' => $request['address_2'] ? $request['address_2'] : NULL,
            'city' => $request['city'] ? $request['city'] : NULL,
            'country' => $request['country'] ? $request['country'] : NULL,
            'password' => $request['password'] ? bcrypt($request['password']) : NULL
        ]);
        $obj = array_filter($obj, 'strlen');
        $this->user->where('id', '=', Auth::user()->id)->update($obj);
        return redirect()->back()->with('message', ['success' => 'yahooo record updated...!' ]);
    }

    public function orders(){

        //------
        $select = ['product_url', 'customs', 'bejeebFees', 'qty', 'price', 'notes', 'actual_price', 'shipping_charges', 'tax_charges',  'orders_products.order_id', 'total_cost', 'status', 'date_created'];
        $ordersArray = \DB::table('orders')
            ->join('orders_products', 'orders_products.order_id', '=', 'orders.order_id')
            ->select($select)
            ->where('orders.uid', '=',  Auth::user()->id)
            ->get();
        //-----
        return View('orders', compact('ordersArray'));
    }

    public function orderSubmit(Request $request){
        $marker = $request['Markers'] ? $request['Markers'] : '';
        if ($marker == '') {
            return redirect()->back()->with('error', ['message' => 'required parameters are missing']);
        }

        $obj = ([
            'phone' => $request->phone,
            'address_1' => $request->address1,
            'address_2' => $request->address2
        ]);
        $this->user->where('id', '=', Auth::user()->id)->update($obj);



        foreach ($marker as $key => $val) {

            $createObj = $this->orders->create([
                'uid' => Auth::user()->id,
                'status' => 'Reviewing',
                'date_created' => Carbon::now()
            ]);

            $this->ordersProducts->create([
                'order_id' => $createObj->id,
                'product_url' => $val['prodURL'],
                'qty' => $val['qty'],
                'notes' => $val['notes']
            ]);
        }
        return $createObj->id;
    }

    public function orderChangeStatus(Request $request){


        if($request->orderStatus == 'Cancelled'){

            $obj = ([
                'status' => $request->orderStatus
            ]);
            $this->orders->where('order_id', '=', $request->orderNumber)->update($obj);
            return 1;
        }
        else {

            $select = ['product_url', 'customs', 'bejeebFees', 'qty', 'price', 'notes', 'actual_price', 'shipping_charges', 'tax_charges',  'orders_products.order_id', 'total_cost', 'status', 'date_created'];
            $orderArray = \DB::table('orders')
                ->join('orders_products', 'orders_products.order_id', '=', 'orders.order_id')
                ->select($select)
                ->where('orders.order_id', '=',  $request->orderNumber)
                ->first();

            $response = $orderArray;

            return view('payments', compact($response));
        }

    }


    public function ckeckoutOrder(Request $request){



            $select = ['product_url', 'customs', 'bejeebFees', 'qty', 'price', 'notes', 'actual_price', 'shipping_charges', 'tax_charges',  'orders_products.order_id', 'total_cost', 'status', 'date_created'];
            $orderArray = \DB::table('orders')
                ->join('orders_products', 'orders_products.order_id', '=', 'orders.order_id')
                ->select($select)
                ->where('orders.order_id', '=',  $request->order_id)
                ->first();

            $response = $orderArray;

            return view('payments', compact('response'));

    }



    public function orderConfirmed(Request $request){


        if($request->payment == 0)
        {
            $status = "Paid (Paypal)";
        }else{

            $status = "Submitted (Cash Collection)";
        }
        $obj = ([
            'status' => $status
        ]);
        $this->orders->where('order_id', '=', $request->order_id)->update($obj);



        return 1;

    }

    public function thanku(Request $request)
    {
        $orderId = $request->order_id;

        return view('thanku', compact('orderId'));
    }



    public function getProductImage(Request $request){


        if($request->prodURL == "")
        {
            return 0;
        }else{
            $html = file_get_contents($request->prodURL);

            $imgTagWrapperId = explode('imgTagWrapperId', $html);
            preg_match_all('/<img[^>]+>/i', $imgTagWrapperId[1], $result);
            $result = explode('src=', $result[0][0]);
            $result = explode('"', $result[1]);

            return $result[1];
        }

    }


}
