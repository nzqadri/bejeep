<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{
    protected $fillable = [
        'order_id', 'product_url', 'qty', 'price', 'notes'
    ];

    public $timestamps = false;
}
